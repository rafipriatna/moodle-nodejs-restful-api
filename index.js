const express = require('express')
const https = require('https');
const axios = require('axios');
const fs = require('fs');
const moment = require('moment');

const app = express();
moment.locale('id');

require('dotenv').config()

const hostname = '127.0.0.1';
const port = 3000;

// GET API Functions
const options = {
    host: process.env.HOST,
    username: process.env.USERNAME,
    password: process.env.PASSWORD,
    semesterSekarang: "PTA 2021/2022"
}

let userData = {
    userId: null,
    token: null
}

let courses = []
let events = []
let fullResources = []

let lastUpdate = null

const showOutput = async () => {
    await getToken()
    await getUserId()
}

const getToken = async () => {
    await axios.get(`${options.host}/login/token.php?username=${options.username}&password=${options.password}&service=moodle_mobile_app`)
        .then(response => {
            userData.token = response.data.token
        })
        .catch(error => {
            console.log(error);
        });
}

const getUserId = async () => {
    await axios.get(`${options.host}/webservice/rest/server.php?wstoken=${userData.token}&wsfunction=core_webservice_get_site_info&moodlewsrestformat=json`).then((response) => {
        userData.userId = response.data.userid

        console.log("\nBerhasil login \n")
        console.log(response.data.sitename)
        console.log("Login sebagai: " + response.data.firstname)
    }, (error) => {
        console.log(error);
    });
}

const getEnrolCourses = async () => {
    await axios.get(`${options.host}/webservice/rest/server.php?wstoken=${userData.token}&wsfunction=core_enrol_get_users_courses&userid=${userData.userId}&moodlewsrestformat=json`).then((response) => {

        const data = response.data
        courses = []

        for (i = 0; i < data.length; i++) {
            const semesterCourse = (response.data[i].fullname).substring(0, 13)

            if (semesterCourse == options.semesterSekarang) {
                courses.push({
                    id: response.data[i].id,
                    nama_matkul: cleanString(response.data[i].fullname, 2),
                    nama_dosen: cleanString(response.data[i].fullname, 3) || '-'
                })
            }
        }

    }, (error) => {
        console.log(error);
    });
}

const getCalendarUpcomingEvents = async () => {
    await axios.get(`${options.host}/webservice/rest/server.php?wstoken=${userData.token}&wsfunction=core_calendar_get_calendar_upcoming_view&moodlewsrestformat=json`).then((response) => {

        const totalEvents = response.data.events.length
        events = []

        for (i = 0; i < courses.length; i++) {
            let dataEvent = []
            for (y = 0; y < totalEvents; y++) {
                if (courses[i].id === response.data.events[y].course.id) {
                    let deskripsi = response.data.events[y].description
                    deskripsi = deskripsi.replace("<p>", "")
                    deskripsi = deskripsi.replace("</p>", "")

                    const ambilTanggal = response.data.events[y].formattedtime
                    let tanggal = ambilTanggal.split('">')[1].split('</a>')[0]
                    const waktu = ambilTanggal.split('</a>, ')[1]
                    // const tanggalBaru = moment(tanggal + " 2021 " + waktu, 'dddd, D MMMM YYYY hh:mm A').format('dddd, D MMMM YYYY HH:mm')
                    dataEvent.push({
                        nama_tugas: response.data.events[y].name,
                        deskripsi: deskripsi,
                        tenggatWaktu: tanggal,
                    })
                }
            }

            events.push({
                nama_matkul: courses[i].nama_matkul,
                nama_dosen: courses[i].nama_dosen,
                tugas: dataEvent
            })
        }
    }, (error) => {
        console.log(error);
    });
}

const getResourceFromCourses = async () => {
    await axios.get(`${options.host}/webservice/rest/server.php?wstoken=${userData.token}&wsfunction=mod_resource_get_resources_by_courses&moodlewsrestformat=json`).then((response) => {

        const data = response.data.resources
        fullResources = []

        for (i = 0; i < courses.length; i++) {
            let resources = []
            for (y = 0; y < data.length; y++) {
                if (courses[i].id === data[y].course) {
                    const namaFile = decodeURIComponent((data[y].contentfiles[0].fileurl).split("/").pop())
                    const dest = `public/files/${namaFile}`

                    if (fs.stat(dest), function(err, stat) {
                        if (err.code == 'ENOENT') download(`${data[y].contentfiles[0].fileurl}?token=${userData.token}`, dest)
                        if (err) console.log(err)
                    })

                    resources.push({
                        nama_file: data[y].name,
                        url: `http://${hostname}:${port}/files/${encodeURIComponent(namaFile)}`
                    })

                }
            }

            fullResources.push({
                nama_matkul: courses[i].nama_matkul,
                files: resources
            })
        }

    }, (error) => {
        console.log(error);
    });
}

const download = function (url, dest) {
    https.get(url, resp => resp.pipe(fs.createWriteStream(dest)));
}

const cleanString = function (str, i) {
    const arr = str.split('|')
    let strClean = []
    arr.map(item => {
        strClean.push(item.trim())
    })

    return strClean[i]
}

// Server
app.use(express.static('public'));

app.get('/update', async (req, res) => {
    await getEnrolCourses()
    await getCalendarUpcomingEvents()
    await getResourceFromCourses()

    lastUpdate = moment().format('LLL');

    res.status(200).json({
        message: "Database updated!"
    })
})

app.get('/matkul', async (req, res) => {
    console.log('Request Matkul')
    res.status(200).json({
        message: `Daftar Mata Kuliah ${options.semesterSekarang}`,
        data: courses,
        lastUpdate: lastUpdate
    })
})

app.get('/tugas', async (req, res) => {
    console.log("Request tugas")
    res.status(200).json({
        message: "Daftar tugas yang akan datang.",
        data: events,
        lastUpdate: lastUpdate
    })
})

app.get('/files', async (req, res) => {
    console.log("Request files")
    res.status(200).json({
        message: "Daftar files.",
        data: fullResources,
        lastUpdate: lastUpdate
    })
})

app.listen(port, () => {
    showOutput()
    console.log(`Server running at http://${hostname}:${port}/`);
})